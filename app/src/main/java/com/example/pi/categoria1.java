package com.example.pi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class categoria1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categoria1);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                fab();

            }
        });

        ListView lista = (ListView) findViewById(R.id.listView);
        final ArrayList<Topico> topicos = AddTopico();
        ArrayAdapter adapter = new Adapter(this, topicos);
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(categoria1.this, Respostas.class);
                intent.putExtra("Resposta", topicos.get(position).getResposta());
                startActivity(intent);

            }
        });
    }

    private ArrayList<Topico> AddTopico(){

        ArrayList<Topico> topicos = new ArrayList<Topico>();

        Topico T = new Topico("Onde posso encontrar o Hospital de Gaspar?", "Você pode encontrar o hospital Nossa Senhora do Perpétuo Socorro na  rua José Krauss, 97 - Sete de Setembro, Gaspar - SC, 89110-000 e para poder entrar em contato com o hospital é só acessar o número: (47) 3332-0109");
        topicos.add(T);

        T = new Topico("Onde posso fazer exames médicos?", "Você pode fazer exames médicos na Unidade de Coleta - Laboratório Santa Catarina, na rua Sete de Setembro, Gaspar - SC, 89114-442 e para  entrar em contato com o laboratório é só acessar o número: (47) 3332-4054");
        topicos.add(T);

        T = new Topico("Onde fica o posto de saúde de Gaspar? ", "Você pode encontrar a Unidade de Saúde Central na rua Vereador Augusto Beduschi, 130 - Centro - Gaspar, SC- CEP: 89110-000 e para entrar em contato com a unidade de saúde é só acessar o número: (47) 3332-9173.");
        topicos.add(T);

        return topicos;
    }

    private void fab() {

        startActivity(new Intent(categoria1.this, AddTopico.class));

    }

}



