package com.example.pi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Respostas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_respostas);


        Intent intent = getIntent();
        String parametro = (String) intent.getSerializableExtra("Resposta");
        TextView resposta = (TextView) findViewById(R.id.resposta);
        resposta.setText(parametro);
    }
}


